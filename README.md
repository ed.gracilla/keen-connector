# Keen.io Connector

## Description
Keen IO is a set of powerful APIs that allow you to collect, analyze, and visualize events from anything connected to the internet. Keen.io Connector Plugin for Reekoh enables you to send device data to a powerful analytics platform for data insights and real-time visualization.

### Configuration

1. Project ID - Your Keen.io project ID.
2. Write Key - The he write key for your Keen.io project.
3. Collection - The collection where data will be sent to.